package de.bots.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class ReplyKeyboardHide extends ReplyMarkup {

	private boolean hide_keyboard = true;

	private boolean selective;

    public boolean isHide_keyboard() {
        return hide_keyboard;
    }

    public void setHide_keyboard(boolean hide_keyboard) {
        this.hide_keyboard = hide_keyboard;
    }

    public boolean isSelective() {
        return selective;
    }

    public void setSelective(boolean selective) {
        this.selective = selective;
    }
}
