package de.bots.model.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import de.bots.TelegramAPI;
import de.bots.model.ReplyMarkup;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SendMessageRequest {
    private Integer chat_id;
    private String text;
    private Boolean disable_web_page_preview;
    private ReplyMarkup reply_markup;
    private TelegramAPI.TextMessageParseMode parse_mode;
    private Integer reply_to_message_id;

    public SendMessageRequest(Integer chat_id, String text, TelegramAPI.TextMessageParseMode parse_mode, Boolean disable_web_page_preview, int reply_to_message_id, ReplyMarkup reply_markup) {
        this.chat_id = chat_id;
        this.text = text;
        this.disable_web_page_preview = disable_web_page_preview;
        this.reply_markup = reply_markup;
        this.parse_mode = parse_mode;
        if (reply_to_message_id != 0)
            this.reply_to_message_id = reply_to_message_id;
    }

    public Integer getChat_id() {
        return chat_id;
    }

    public void setChat_id(Integer chat_id) {
        this.chat_id = chat_id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public TelegramAPI.TextMessageParseMode getParse_mode() {
        return parse_mode;
    }

    public void setParse_mode(TelegramAPI.TextMessageParseMode parse_mode) {
        this.parse_mode = parse_mode;
    }

    public ReplyMarkup getReply_markup() {
        return reply_markup;
    }

    public void setReply_markup(ReplyMarkup reply_markup) {
        this.reply_markup = reply_markup;
    }

    public Boolean getDisable_web_page_preview() {
        return disable_web_page_preview;
    }

    public void setDisable_web_page_preview(Boolean disable_web_page_preview) {
        this.disable_web_page_preview = disable_web_page_preview;
    }

    public Integer getReply_to_message_id() {
        return reply_to_message_id;
    }

    public void setReply_to_message_id(Integer reply_to_message_id) {
        this.reply_to_message_id = reply_to_message_id;
    }
}
