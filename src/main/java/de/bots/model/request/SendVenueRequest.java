package de.bots.model.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import de.bots.model.ReplyMarkup;
import de.bots.model.Venue;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SendVenueRequest {

	private Integer chat_id;
	private float latitude;
	private float longitude;
	private String title;
	private String address;
	private String foursquare_id;
	private boolean disable_notification;
	private Integer reply_to_message_id;
	private ReplyMarkup reply_markup;

	public SendVenueRequest(Integer chat_id, Venue venue, boolean disable_notification,
		Integer reply_to_message_id, ReplyMarkup reply_markup) {
		this.chat_id = chat_id;
		this.latitude = venue.getLocation().getLatitude();
		this.longitude = venue.getLocation().getLongitude();
		this.title = venue.getTitle();
		this.address = venue.getAddress();
		this.foursquare_id = venue.getFoursquare_id();
		this.disable_notification = disable_notification;
		this.reply_to_message_id = reply_to_message_id;
		this.reply_markup = reply_markup;
	}

	public Integer getChat_id() {
		return chat_id;
	}

	public void setChat_id(Integer chat_id) {
		this.chat_id = chat_id;
	}

	public float getLatitude() {
		return latitude;
	}

	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}

	public float getLongitude() {
		return longitude;
	}

	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getFoursquare_id() {
		return foursquare_id;
	}

	public void setFoursquare_id(String foursquare_id) {
		this.foursquare_id = foursquare_id;
	}

	public boolean isDisable_notification() {
		return disable_notification;
	}

	public void setDisable_notification(boolean disable_notification) {
		this.disable_notification = disable_notification;
	}

	public Integer getReply_to_message_id() {
		return reply_to_message_id;
	}

	public void setReply_to_message_id(Integer reply_to_message_id) {
		this.reply_to_message_id = reply_to_message_id;
	}

	public ReplyMarkup getReply_markup() {
		return reply_markup;
	}

	public void setReply_markup(ReplyMarkup reply_markup) {
		this.reply_markup = reply_markup;
	}
}
