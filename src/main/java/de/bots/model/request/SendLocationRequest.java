package de.bots.model.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import de.bots.model.Location;
import de.bots.model.ReplyMarkup;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SendLocationRequest {
    private Integer chat_id;
    private float latitude;
    private float longitude;
    private Boolean disable_notification;
    private Integer reply_to_message_id;
    private ReplyMarkup reply_markup;

    public SendLocationRequest(int chat_id, Location location, boolean disable_notification, int reply_to_message_id, ReplyMarkup reply_markup) {
        this.chat_id = chat_id;
        this.latitude = location.getLatitude();
        this.longitude = location.getLongitude();
        this.disable_notification = disable_notification;
        if (reply_to_message_id != 0) this.reply_to_message_id = reply_to_message_id;
        this.reply_markup = reply_markup;
    }

    public Integer getChat_id() {
        return chat_id;
    }

    public void setChat_id(Integer chat_id) {
        this.chat_id = chat_id;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    @JsonIgnore
    public Location getLocation() {
        Location loc = new Location(this.longitude, this.latitude);
        return loc;
    }

    public void setLocation(Location location) {
        this.latitude = location.getLatitude();
        this.longitude = location.getLongitude();
    }

    public Boolean getDisable_notification() {
        return disable_notification;
    }

    public void setDisable_notification(Boolean disable_notification) {
        this.disable_notification = disable_notification;
    }

    public Integer getReply_to_message_id() {
        return reply_to_message_id;
    }

    public void setReply_to_message_id(Integer reply_to_message_id) {
        this.reply_to_message_id = reply_to_message_id;
    }

    public ReplyMarkup getReply_markup() {
        return reply_markup;
    }

    public void setReply_markup(ReplyMarkup reply_markup) {
        this.reply_markup = reply_markup;
    }
}
