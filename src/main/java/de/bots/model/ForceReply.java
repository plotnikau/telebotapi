package de.bots.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ForceReply extends ReplyMarkup {

	private boolean force_reply;

	private boolean selective;

    public boolean isSelective() {
        return selective;
    }

    public void setSelective(boolean selective) {
        this.selective = selective;
    }

    public boolean isForce_reply() {

        return force_reply;
    }

    public void setForce_reply(boolean force_reply) {
        this.force_reply = force_reply;
    }
}
