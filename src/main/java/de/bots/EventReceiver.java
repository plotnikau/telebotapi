package de.bots;

import de.bots.model.Update;
import de.bots.model.response.UpdateResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

@Component
public class EventReceiver {

    private Collection<Command> commands;
    private Collection<DefaultHandler> defaultHandlers;

    @Autowired
    private TelegramAPI telegramAPI;

    @Autowired
    private ApplicationContext applicationContext;

    private AtomicLong eventID = new AtomicLong(0L);

    private static final Logger LOG = Logger.getLogger(EventReceiver.class);

    //TODO This scheduler runs every second but telegramAPI.getUpdates has a timeout of 5 seconds. Check if this is OK.
    @Scheduled(fixedRate = 1000L)
    public void handleEvents() {
        UpdateResponse updateResponse = telegramAPI.getUpdates(eventID.longValue());

        //TODO move to "post construct"
        initCommands();

        if (updateResponse.isOk()) {
            Update[] updates = updateResponse.getResult();
            for (Update update : updates) {
                handleUpdate(update);


                eventID.set(update.getUpdate_id() + 1);
            }
        }
    }

    private void handleUpdate(Update update) {
        LOG.info("Received update: " + update.getUpdate_id());
        if(update.getMessage() == null) return;
        int chatId = update.getMessage().getChat().getId();

        // the commands have to check if they can handle text messages only
        if (update.getMessage().getText()==null) {
            LOG.warn("non text message received");
            //telegramAPI.sendMessage(chatId, "I'm a Text-Only-Bot, sorry.");
            //return;
        }

        Optional<Command> activeCommand = commands.stream().filter(cmd -> cmd.isActive(chatId)).findFirst();
        if (activeCommand.isPresent()) {
            try {
                activeCommand.get().handle(update);
            }
            catch (Exception e) {
                LOG.error("Exception when trying to handle command ", e);
                telegramAPI.sendMessage(chatId, "Sorry, something went wrong, try again!");
            }
        }
        else {
            Optional<Command> handlingCommand = commands.stream().filter(cmd -> cmd.canHandle(chatId, update)).findFirst();
            Optional<DefaultHandler> defaultHandler = defaultHandlers.stream().filter(hdl -> hdl.canHandle(chatId, update)).findFirst();
            if (handlingCommand.isPresent()) {
                try {
                    handlingCommand.get().handle(update);
                }
                catch (Exception e) {
                    LOG.error("Exception when trying to handle command ", e);
                    telegramAPI.sendMessage(chatId, "Sorry, something went wrong, try again!");
                }
            }
            else if (defaultHandler.isPresent()) {
                try {
                    defaultHandler.get().handle(update);
                }
                catch (Exception e) {
                    LOG.error("Exception when trying to handle command by default handler", e);
                    telegramAPI.sendMessage(chatId, "Sorry, something went wrong, try again!");
                }
            }
            else {
                telegramAPI.sendMessage(chatId, "I don't understand that, sorry.");
            }
        }
    }

    private void initCommands() {
        if (commands == null) {
            commands = applicationContext.getBeansOfType(Command.class).values();
            for (Command command : commands) {
                LOG.info("Found command " + command.getClass().getSimpleName());
            }

        }
        if (defaultHandlers == null) {
            defaultHandlers = applicationContext.getBeansOfType(DefaultHandler.class).values();
            for (DefaultHandler defaultHandler : defaultHandlers) {
                LOG.info("Found default handler " + defaultHandler.getClass().getSimpleName());
            }

        }
    }

}
