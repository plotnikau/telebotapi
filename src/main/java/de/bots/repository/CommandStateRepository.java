package de.bots.repository;

import de.bots.Command;

public interface CommandStateRepository {
    public void setState(Command command, int chatId, String key, String value);

    public String getState(Command command, int chatId, String key);

    public void deleteState(Command command, int chatId, String key);
}
