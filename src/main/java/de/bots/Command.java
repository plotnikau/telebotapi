package de.bots;

import de.bots.model.Update;

public interface Command {
    boolean isActive(int chatId);
    boolean canHandle(int chatId, Update update);

    void handle(Update update);
}
