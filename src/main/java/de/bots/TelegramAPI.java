package de.bots;

import de.bots.model.*;
import de.bots.model.request.SendLocationRequest;
import de.bots.model.request.SendMessageRequest;
import de.bots.model.request.SendVenueRequest;
import de.bots.model.response.FileResponse;
import de.bots.model.response.MessageResponse;
import de.bots.model.response.UpdateResponse;
import de.bots.model.response.UserResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

@Component
public class TelegramAPI {
    public enum TextMessageParseMode {
        MARKDOWN("Markdown"), HTML("HTML");
        private final String text;
        private TextMessageParseMode(final String text) {
            this.text = text;
        }
        @Override
        public String toString() {
            return text;
        }
    }

	public static boolean commandMatch(String messageText, String command) {
		if (messageText!=null && command!=null) {
			return messageText.toLowerCase().startsWith(command);
		} else return false;
	}
	public static boolean commandMatch(Update update, String command) {
		return commandMatch(update.getMessage().getText(), command);
	}

	private String apiBaseUrl;
	private RestTemplate restTemplate;

	@Autowired
	public TelegramAPI(BotProperties botProperties) {
		apiBaseUrl = "https://api.telegram.org/bot" + botProperties.getApiKey() + "/";
	}

	private RestTemplate getRestTemplate() {
		if (null == restTemplate) {
			restTemplate = new RestTemplate();
		}
		return restTemplate;
	}

	private URI getSendMessageURI() {
		return URI.create(apiBaseUrl + "sendMessage");
	}

    private URI getSendLocationURI() {
        return URI.create(apiBaseUrl + "sendLocation");
    }

	private URI getSendPhotoURI() {
		return URI.create(apiBaseUrl + "sendPhoto");
	}

	private URI getUpdatesURI() {
		return URI.create(apiBaseUrl + "getUpdates");
	}

	private URI getSendVenueURI() {
		return URI.create(apiBaseUrl + "sendVenue");
	}

	protected URI getFileURI() {
		return URI.create(apiBaseUrl + "getFile");
	}

	public UserResponse getMe() {
		return getRestTemplate().getForObject(URI.create(apiBaseUrl + "getMe"), UserResponse.class);
	}

	public UpdateResponse getUpdates() {
		return getRestTemplate().getForObject(getUpdatesURI(), UpdateResponse.class);
	}

	public UpdateResponse getUpdates(Long offset) {
        URI targetUrl = UriComponentsBuilder.fromUri(getUpdatesURI())
                .queryParam("offset", offset.toString())
				.queryParam("timeout", "5")
                .build()
                .toUri();

		return getRestTemplate().getForObject(targetUrl, UpdateResponse.class);
	}

	public FileResponse getFile(String file_id) {
		URI targetUrl = UriComponentsBuilder.fromUri(getUpdatesURI())
				.queryParam("offset", file_id)
				.build()
				.toUri();

		return getRestTemplate().getForObject(targetUrl, FileResponse.class);
	}


	public MessageResponse sendMessage(int chat_id, String text) {
		return sendMessageInternal(chat_id, text, null, false, 0, null);
	}

    public MessageResponse sendMessage(int chat_id, String text, TextMessageParseMode parse_mode) {
        return sendMessageInternal(chat_id, text, parse_mode, false, 0, null);
    }

    public MessageResponse sendMessage(int chat_id, String text, boolean disable_web_page_preview, int reply_to_message_id, ReplyMarkup reply_markup) {
		return sendMessageInternal(chat_id, text, null, disable_web_page_preview, reply_to_message_id, reply_markup);
	}

	public MessageResponse sendMessage(int chat_id, String text, TextMessageParseMode parse_mode, boolean disable_web_page_preview, int reply_to_message_id, ReplyMarkup reply_markup) {
		return sendMessageInternal(chat_id, text, parse_mode, disable_web_page_preview, reply_to_message_id, reply_markup);
	}

	private MessageResponse sendMessageInternal(int chat_id, String text, TextMessageParseMode parse_mode, boolean disable_web_page_preview, int reply_to_message_id, ReplyMarkup reply_markup) {
		SendMessageRequest request = new SendMessageRequest(chat_id, text, parse_mode, disable_web_page_preview, reply_to_message_id, reply_markup);
		return getRestTemplate().postForObject(getSendMessageURI(), request, MessageResponse.class);
	}


	public  MessageResponse sendLocation(int chat_id, Location location) {
		return sendLocationInternal(chat_id, location, false, 0, null);
	}
/*
    private MessageResponse sendLocationInternal(int chat_id, Location location, boolean disable_notification, int reply_to_message_id, ReplyMarkup reply_markup) {
        SendLocationRequest request = new SendLocationRequest(chat_id, location, disable_notification, reply_to_message_id, reply_markup);
        return getRestTemplate().postForObject(getSendLocationURI(), request, MessageResponse.class);
    }
*/
	private MessageResponse sendLocationInternal(int chat_id, Location location, boolean disable_notification, int reply_to_message_id, ReplyMarkup reply_markup) {
		SendLocationRequest request = new SendLocationRequest(chat_id, location, disable_notification, reply_to_message_id, reply_markup);
        RestTemplate rt = getRestTemplate();
        URI uri = getSendLocationURI();
		return rt.postForObject(uri, request, MessageResponse.class);
	}

	public MessageResponse sendVenue(int chat_id, Venue venue) {
		return sendVenueInternal(chat_id, venue, false, 0, null);
	}

	private MessageResponse sendVenueInternal(int chat_id, Venue venue, boolean disable_notification, int reply_to_message_id, ReplyMarkup reply_markup) {
		SendVenueRequest sendVenueRequest = new SendVenueRequest(chat_id, venue, disable_notification, reply_to_message_id, reply_markup);
		RestTemplate restTemplate = getRestTemplate();
		URI uri = getSendVenueURI();
		return restTemplate.postForObject(uri, sendVenueRequest, MessageResponse.class);
	}

	public MessageResponse sendPhoto(int chat_id, URL imageUrl, File tempFile) throws IOException {
		return sendPhotoInternal(chat_id, imageUrl, tempFile, null, 0, null);
	}

	public MessageResponse sendPhoto(int chat_id, URL imageUrl, File tempFile, String caption) throws IOException {
		return sendPhotoInternal(chat_id, imageUrl, tempFile, caption, 0, null);
	}

	public MessageResponse sendPhoto(int chat_id, URL imageUrl, File tempFile, String caption, int reply_to_message_id,
			ReplyKeyboardMarkup reply_markup) throws IOException {
		return sendPhotoInternal(chat_id, imageUrl, tempFile, caption, reply_to_message_id, reply_markup);
	}

	public MessageResponse sendPhoto(int chat_id, URL imageUrl, File tempFile, String caption, int reply_to_message_id,
			ReplyKeyboardHide reply_markup) throws IOException {
		return sendPhotoInternal(chat_id, imageUrl, tempFile, caption, reply_to_message_id, reply_markup);
	}

	public MessageResponse sendPhoto(int chat_id, URL imageUrl, File tempFile, String caption, int reply_to_message_id,
			ForceReply reply_markup) throws IOException {
		return sendPhotoInternal(chat_id, imageUrl, tempFile, caption, reply_to_message_id, reply_markup);
	}

	public MessageResponse resendPhoto(int chatId, String fileId, String caption) throws IOException {
		return resendPhotoInternal(chatId, fileId, caption, 0, null);
	}

	private MessageResponse sendPhotoInternal(int chat_id, URL imageUrl, File tempFile, String caption,
			int reply_to_message_id, Object reply_markup) throws IOException {

		HttpEntity<Object> request = createImageRequest(chat_id, imageUrl, tempFile, caption, reply_to_message_id,
				reply_markup);

		return getRestTemplate().postForObject(getSendPhotoURI(), request, MessageResponse.class);
	}

	private MessageResponse resendPhotoInternal(int chat_id, String fileId, String caption,
			int reply_to_message_id, Object reply_markup) throws IOException {

		HttpEntity<Object> request = createImageRequest(chat_id, fileId, caption, reply_to_message_id, reply_markup);

		return getRestTemplate().postForObject(getSendPhotoURI(), request, MessageResponse.class);
	}

	private HttpEntity<Object> createImageRequest(int chat_id, URL imageUrl, File tempFile, String caption,
			int reply_to_message_id, Object reply_markup) throws IOException {
		Files.copy(imageUrl.openStream(), tempFile.toPath(), StandardCopyOption.REPLACE_EXISTING);

		Resource photoResource = new FileSystemResource(tempFile);

		MultiValueMap<String, Object> data = new LinkedMultiValueMap<String, Object>();
		data.add("chat_id", chat_id);
		data.add("photo", photoResource);
		data.add("caption", caption);
		if (reply_to_message_id != 0) {
			data.add("reply_to_message_id", reply_to_message_id);
		}
		if (reply_markup != null) {
			data.add("reply_markup", reply_markup);
		}

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		return new HttpEntity<Object>(data, headers);
	}

	private HttpEntity<Object> createImageRequest(int chat_id, String file_id, String caption, int reply_to_message_id, Object reply_markup) throws IOException {
		MultiValueMap<String, Object> data = new LinkedMultiValueMap<String, Object>();
		data.add("chat_id", chat_id);
		data.add("photo", file_id);
		data.add("caption", caption);
		if (reply_to_message_id != 0) {
			data.add("reply_to_message_id", reply_to_message_id);
		}
		if (reply_markup != null) {
			data.add("reply_markup", reply_markup);
		}

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		return new HttpEntity<Object>(data, headers);
	}
}
