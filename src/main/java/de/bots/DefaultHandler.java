package de.bots;

import de.bots.model.Update;

public interface DefaultHandler {

    boolean canHandle(int chatId, Update update);

    void handle(Update update);
}
