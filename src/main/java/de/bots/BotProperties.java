package de.bots;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("bot")
public class BotProperties {

	private String apiKey = "apikey";

	private String botName = "nameofBot";

	private String redisUri = "localhost";

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String greeting) {
		this.apiKey = greeting;
	}

	public String getBotName() {
		return botName;
	}

	public void setBotName(String botName) {
		this.botName = botName;
	}

	public String getRedisUri() {
		return redisUri;
	}

	public void setRedisUri(String redisUri) {
		this.redisUri = redisUri;
	}
}